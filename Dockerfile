FROM openjdk:17
ADD /target/yandex-academy-0.0.1-SNAPSHOT.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]