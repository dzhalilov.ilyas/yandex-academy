INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('863e1a7a-1304-42ae-943b-179184c077e3', '2022-02-02 12:00:00.000000', 'jPhone 13', 'd515e43f-f3f6-4471-bb77-6b455017a2d2', 79999, 0);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4', '2022-02-02 12:00:00.000000', 'Xomiа Readme 10', 'd515e43f-f3f6-4471-bb77-6b455017a2d2', 59999, 0);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('d515e43f-f3f6-4471-bb77-6b455017a2d2', '2022-02-02 12:00:00.000000', 'Смартфоны', '069cb8d7-bbdd-47d3-ad8f-82ef4c269df1', 69999, 1);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('98883e8f-0507-482f-bce2-2fb306cf6483', '2022-02-03 12:00:00.000000', 'Samson 70" LED UHD Smart', '1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', 32999, 0);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('74b81fda-9cdc-4b63-8927-c978afed5cf4', '2022-02-03 12:00:00.000000', 'Phyllis 50" LED UHD Smarter', '1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', 49999, 0);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('73bc3b36-02d1-4245-ab35-3106c9ee1c65', '2022-02-03 15:00:00.000000', 'Goldstar 65" LED UHD LOL Very Smart', '1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', 69999, 0);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', '2022-02-03 15:00:00.000000', 'Телевизоры', '069cb8d7-bbdd-47d3-ad8f-82ef4c269df1', 50999, 1);
INSERT INTO public.shop_unit (id, date, name, parent_id, price, type) VALUES ('069cb8d7-bbdd-47d3-ad8f-82ef4c269df1', '2022-02-03 15:00:00.000000', 'Товары', null, 58599, 1);

INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('069cb8d7-bbdd-47d3-ad8f-82ef4c269df1', 'd515e43f-f3f6-4471-bb77-6b455017a2d2');
INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('d515e43f-f3f6-4471-bb77-6b455017a2d2', '863e1a7a-1304-42ae-943b-179184c077e3');
INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('d515e43f-f3f6-4471-bb77-6b455017a2d2', 'b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4');
INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('069cb8d7-bbdd-47d3-ad8f-82ef4c269df1', '1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2');
INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', '74b81fda-9cdc-4b63-8927-c978afed5cf4');
INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', '98883e8f-0507-482f-bce2-2fb306cf6483');
INSERT INTO public.shop_unit_children (shop_unit_id, children_id) VALUES ('1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2', '73bc3b36-02d1-4245-ab35-3106c9ee1c65');

INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-01 12:00:00.000000', null, '069cb8d7-bbdd-47d3-ad8f-82ef4c269df1');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-02 12:00:00.000000', 69999, 'd515e43f-f3f6-4471-bb77-6b455017a2d2');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-02 12:00:00.000000', 59999, 'b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-02 12:00:00.000000', 69999, '069cb8d7-bbdd-47d3-ad8f-82ef4c269df1');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-02 12:00:00.000000', 79999, '863e1a7a-1304-42ae-943b-179184c077e3');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 12:00:00.000000', 49999, '74b81fda-9cdc-4b63-8927-c978afed5cf4');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 12:00:00.000000', 41499, '1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 12:00:00.000000', 55749, '069cb8d7-bbdd-47d3-ad8f-82ef4c269df1');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 12:00:00.000000', 32999, '98883e8f-0507-482f-bce2-2fb306cf6483');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 15:00:00.000000', 69999, '73bc3b36-02d1-4245-ab35-3106c9ee1c65');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 15:00:00.000000', 50999, '1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2');
INSERT INTO public.shop_unit_history (date, price, shop_unit_id) VALUES ('2022-02-03 15:00:00.000000', 58599, '069cb8d7-bbdd-47d3-ad8f-82ef4c269df1');