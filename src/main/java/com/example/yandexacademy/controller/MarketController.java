package com.example.yandexacademy.controller;


import com.example.yandexacademy.exception.Error;
import com.example.yandexacademy.exception.NotFoundUnitException;
import com.example.yandexacademy.model.ShopUnit;
import com.example.yandexacademy.service.MarketService;
import com.example.yandexacademy.to.ShopUnitImportRequest;
import com.example.yandexacademy.to.ShopUnitStatisticUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

import static com.example.yandexacademy.util.ValidationUtil.UUID_REGEX;

@RestController
@Slf4j
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class MarketController {

    public static final String VALIDATION_FAILED_MESSAGE = "Validation Failed";
    public static final String ITEM_NOT_FOUND_MESSAGE = "Item not found";
    public static final Error NOT_FOUND_NODES = new Error(404, ITEM_NOT_FOUND_MESSAGE);
    public static final Error VALIDATION_FAILED = new Error(400, VALIDATION_FAILED_MESSAGE);


    @Autowired
    MarketService service;

    @PostMapping("/imports")
    public ResponseEntity<?> importData(@Valid @RequestBody ShopUnitImportRequest request) {
        log.info("Starting import data");
        service.importData(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/nodes/{id}")
    public ResponseEntity<?> getItem(@Pattern(regexp = UUID_REGEX) @PathVariable String id) {
        log.info("Get info for item id={}", id);
        ShopUnit unit = service.getItem(id);
        log.info("Received info: {}", unit);
        if (unit == null) {
            throw new NotFoundUnitException(ITEM_NOT_FOUND_MESSAGE);
        }
        return ResponseEntity.status(HttpStatus.OK).body(unit);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteItem(@Pattern(regexp = UUID_REGEX) @PathVariable String id) {
        log.info("Delete item id={}", id);
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/sales")
    public ResponseEntity<?> getSales(@NotNull @RequestParam("date") LocalDateTime date) {
        log.info("Get sales for date={}", date);
        List<ShopUnitStatisticUnit> sales = service.getSales(date);
        return ResponseEntity.status(HttpStatus.OK).body(sales);
    }

    @GetMapping("/node/{id}/statistic")
    public ResponseEntity<?> getStatistic(@Pattern(regexp = UUID_REGEX) @PathVariable String id,
                                          @Nullable @RequestParam("dateStart") LocalDateTime dateStart,
                                          @Nullable @RequestParam("dateEnd") LocalDateTime dateEnd) {
        log.info("Get statistic item id={} between dates [{} - {}]", id, dateStart, dateEnd);
        List<ShopUnitStatisticUnit> sales = service.getStatistic(id, dateStart, dateEnd);
        return ResponseEntity.status(HttpStatus.OK).body(sales);
    }
}
