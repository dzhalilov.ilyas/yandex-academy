package com.example.yandexacademy.service;

import com.example.yandexacademy.exception.IllegalRequestDataException;
import com.example.yandexacademy.exception.NotFoundUnitException;
import com.example.yandexacademy.model.PriceCategoryCounter;
import com.example.yandexacademy.model.ShopUnit;
import com.example.yandexacademy.model.ShopUnitHistory;
import com.example.yandexacademy.model.ShopUnitType;
import com.example.yandexacademy.repository.HistoryRepository;
import com.example.yandexacademy.repository.ShopUnitRepository;
import com.example.yandexacademy.to.ShopUnitImportRequest;
import com.example.yandexacademy.to.ShopUnitStatisticUnit;
import com.example.yandexacademy.util.ShopUnitConverter;
import com.example.yandexacademy.util.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;

import static com.example.yandexacademy.controller.MarketController.ITEM_NOT_FOUND_MESSAGE;
import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED_MESSAGE;
import static com.example.yandexacademy.util.ShopUnitConverter.convertToStatisticUnit;

@Service
public class MarketService {

    @Autowired
    ShopUnitRepository shopUnitRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Transactional
    public void importData(ShopUnitImportRequest request) {
        List<ShopUnit> shopUnits = ShopUnitConverter.convertToShopUnit(request.getItems(), request.getUpdateDate());
        ValidationUtil.valid(shopUnits);
        checkParent(shopUnits);
        // Save or update entity
        for (ShopUnit shopUnit : shopUnits) {
            Optional<ShopUnit> optionalShopUnit = shopUnitRepository.findById(shopUnit.getId());
            if (optionalShopUnit.isPresent()) {
                shopUnitRepository.updateValues(shopUnit.getId(), shopUnit.getName(), shopUnit.getDate(),
                        shopUnit.getPrice(), shopUnit.getParentId());
            } else {
                shopUnitRepository.save(shopUnit);
            }
        }

        addChildren(shopUnits);
        // count and set Category price
        Map<String, Integer> catalogPriceMap = new HashMap<>();
        shopUnits.forEach(s -> catalogPriceMap.put(s.getId(), s.getPrice()));
        for (ShopUnit unit : shopUnits) {
            Map<String, Integer> idAndPriceMap = setCategoriesPriceAndDate(unit, request.getUpdateDate());
            catalogPriceMap.putAll(idAndPriceMap);
        }
        // save history
        addHistory(catalogPriceMap, request.getUpdateDate());
    }

    private void addHistory(Map<String, Integer> catalogPriceMap, LocalDateTime updateDate) {
        for (Map.Entry<String, Integer> item : catalogPriceMap.entrySet()) {
            Optional<ShopUnit> shopUnit = shopUnitRepository.findById(item.getKey());
            historyRepository.save(new ShopUnitHistory(shopUnit.get(), updateDate, item.getValue()));
        }
    }

    private Optional<ShopUnit> giveParent(ShopUnit shopUnit) {
        return shopUnit.getParentId() != null ? shopUnitRepository.findById(shopUnit.getParentId()) : Optional.empty();
    }

    private Map<String, Integer> setCategoriesPriceAndDate(ShopUnit unit, LocalDateTime dateTime) {
        Map<String, Integer> catalogPriceMap = new HashMap<>();
        while (unit.getParentId() != null) {
            Optional<ShopUnit> parentUnit = shopUnitRepository.findById(unit.getParentId());
            if (parentUnit.isPresent()) {
                Integer price = checkDivisionByZero(countCategoryPrice(unit.getParentId(), unit));
                shopUnitRepository.updatePriceAndDate(parentUnit.get().getId(), dateTime, price);
                catalogPriceMap.put(parentUnit.get().getId(), price);
                unit = parentUnit.get();
            } else {
                break;
            }
        }
        return catalogPriceMap;
    }

    private void addChildren(List<ShopUnit> shopUnits) {
        for (ShopUnit unit : shopUnits) {
            if (unit.getParentId() != null) {
                boolean replaced = false;
                Optional<ShopUnit> byId = shopUnitRepository.findById(unit.getParentId());
                if (byId.isPresent()) {
                    Set<ShopUnit> children = byId.get().getChildren();
                    for (ShopUnit child : children) {
                        if (child.getId().equals(unit.getId())) {
                            child.setName(unit.getName());
                            child.setDate(unit.getDate());
                            child.setPrice(unit.getPrice());
                            child.setParentId(unit.getParentId());
                            replaced = true;
                            break;
                        }
                    }
                    if (!replaced) {
                        children.add(unit);
                    }
                }
            }
        }
    }

    private void checkParent(List<ShopUnit> shopUnits) {
        for (ShopUnit unit : shopUnits) {
            if (unit.getParentId() != null) {
                Optional<ShopUnit> byId = shopUnitRepository.findById(unit.getParentId());
                if (byId.isPresent() && byId.get().getType() == ShopUnitType.OFFER) {
                    throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
                }
            }
            if (unit.getType() == ShopUnitType.OFFER) {
                Optional<ShopUnit> byId = shopUnitRepository.findById(unit.getId());
                if (byId.isPresent()) {
                    if (byId.get().getChildren() == null || byId.get().getChildren().size() == 0) {

                    } else {
                        throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
                    }
                }
            }
        }
    }


    private PriceCategoryCounter countCategoryPrice(String id, ShopUnit shopUnit) {
        Set<ShopUnit> unitList = shopUnitRepository.findShopUnitsByParentId(id);
        PriceCategoryCounter counter = new PriceCategoryCounter(0, 0);
        for (ShopUnit unit : unitList) {
            if (unit.getType() == ShopUnitType.CATEGORY) {
                PriceCategoryCounter temp = countCategoryPrice(unit.getId(), shopUnit);
                counter.setPrice(counter.getPrice() + temp.getPrice());
                counter.setCount(counter.getCount() + temp.getCount());
            } else {
                counter.setPrice(counter.getPrice() + unit.getPrice());
                counter.setCount(counter.getCount() + 1);
            }
        }
        return counter;
    }

    private Integer checkDivisionByZero(PriceCategoryCounter counter) {
        return counter.getCount() != 0 ? counter.getPrice() / counter.getCount() : null;
    }

    private void removeEmptyChildren(ShopUnit unit) {
        if (unit.getType() == ShopUnitType.OFFER) {
            unit.setChildren(null);
        } else if (unit.getChildren().size() > 0) {
            for (ShopUnit child : unit.getChildren()) {
                removeEmptyChildren(child);
            }
        }
    }

    public ShopUnit getItem(String id) {
        ShopUnit unit = shopUnitRepository.findById(id).orElse(null);
        if (unit != null) {
            removeEmptyChildren(unit);
        }
        return unit;
    }

    @Transactional
    public void delete(String id) {
        Optional<ShopUnit> optional = shopUnitRepository.findById(id);
        if (optional.isEmpty()) {
            throw new NotFoundUnitException(ITEM_NOT_FOUND_MESSAGE);
        } else {
            deleteItems(optional.get());
            setCategoriesPriceAndDate(optional.get(), optional.get().getDate());
        }
    }

    private void deleteItems(ShopUnit unit) {
        String parentId = unit.getParentId();
        shopUnitRepository.deleteById(unit.getId());
        if (parentId != null) {
            Optional<ShopUnit> optionalParent = shopUnitRepository.findById(parentId);
            if (optionalParent.isPresent()) {
                ShopUnit parent = optionalParent.get();
                Set<ShopUnit> children = parent.getChildren();
                children.remove(unit);
            }
        }
    }

    public List<ShopUnitStatisticUnit> getSales(LocalDateTime date) {
        List<ShopUnit> shopUnits = getShopUnitsBetweenDateTimes(date.minus(Period.ofDays(1)), date);
        return convertToStatisticUnit(shopUnits);
    }

    private List<ShopUnit> getShopUnitsBetweenDateTimes(LocalDateTime startDate, LocalDateTime endDate) {
        return shopUnitRepository.findAllByDateBetween(startDate, endDate);
    }

    public List<ShopUnitStatisticUnit> getStatistic(String id, LocalDateTime dateStart, LocalDateTime dateEnd) {
        Optional<ShopUnit> optionalShopUnit = shopUnitRepository.findById(id);
        if (optionalShopUnit.isEmpty()) {
            throw new NotFoundUnitException(ITEM_NOT_FOUND_MESSAGE);
        }
        List<ShopUnitHistory> historyList;
        if (dateStart == null && dateEnd == null) {
            historyList = historyRepository.findAllByShopId(id);
        } else {
            dateStart = dateStart == null ? LocalDateTime.of(2000, 1, 1, 0, 0) : dateStart;
            dateEnd = dateEnd == null ? LocalDateTime.of(2000, 1, 1, 0, 0) : dateEnd;
            historyList = historyRepository.findAllByShopUnitIdAndDateBetween(id, dateStart, dateEnd);
        }
        if (historyList.isEmpty()) {
            throw new NotFoundUnitException(ITEM_NOT_FOUND_MESSAGE);
        }
        return convertToStatisticUnit(optionalShopUnit.get(), historyList);
    }
}
