package com.example.yandexacademy.repository;

import com.example.yandexacademy.model.ShopUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Transactional(readOnly = true)
public interface ShopUnitRepository extends JpaRepository<ShopUnit, String> {

    @Query("select s from ShopUnit s where s.parentId =?1")
    Set<ShopUnit> findShopUnitsByParentId(String id);

    @Transactional
    void deleteById(String id);

    List<ShopUnit> findAllByDateBetween(LocalDateTime startDate, LocalDateTime endDate);

    @Modifying
    @Transactional
    @Query("update ShopUnit s set s.name =?2, s.date =?3, s.price =?4, s.parentId =?5 where s.id =?1")
    void updateValues(String id, String name, LocalDateTime dateTime, Integer price, String parentId);

    @Modifying
    @Transactional
    @Query("update ShopUnit s set s.date =?2, s.price =?3 where s.id =?1")
    void updatePriceAndDate(String id, LocalDateTime dateTime, Integer price);

    @Query("select s.children from ShopUnit s where s.id =?1")
    Set<ShopUnit> getChildrenById(String id);
}
