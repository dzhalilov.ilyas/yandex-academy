package com.example.yandexacademy.repository;

import com.example.yandexacademy.model.ShopUnitHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface HistoryRepository extends JpaRepository<ShopUnitHistory, Integer> {
    List<ShopUnitHistory> findAllByShopUnitIdAndDateBetween(String id, LocalDateTime dateStart, LocalDateTime dateEnd);

    @Query("select h from ShopUnitHistory h where h.shopUnitId =?1 order by h.date DESC")
    List<ShopUnitHistory> findAllByShopId(String id);
}
