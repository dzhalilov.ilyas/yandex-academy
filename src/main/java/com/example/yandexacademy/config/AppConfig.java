package com.example.yandexacademy.config;

import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

import static com.example.yandexacademy.util.DateTimeFormatters.CUSTOM_DATE_TIME_FORMAT;

@Configuration
public class AppConfig {
    //    https://www.baeldung.com/spring-boot-formatting-json-dates
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(CUSTOM_DATE_TIME_FORMAT);
            builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(CUSTOM_DATE_TIME_FORMAT)));
        };
    }
}
