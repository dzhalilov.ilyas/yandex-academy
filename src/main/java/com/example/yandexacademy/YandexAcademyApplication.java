package com.example.yandexacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YandexAcademyApplication {

    public static void main(String[] args) {
        SpringApplication.run(YandexAcademyApplication.class, args);
    }

}
