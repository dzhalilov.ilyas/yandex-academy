package com.example.yandexacademy.util;

import lombok.NonNull;
import org.springframework.expression.ParseException;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateTimeFormatters {
    public static final String CUSTOM_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    @Component
    public static class LocalDateFormatter implements Formatter<LocalDateTime> {

        @Override
        public LocalDateTime parse(@NonNull String text, Locale locale) throws ParseException {
            return StringUtils.hasLength(text) ? LocalDateTime.parse(text,
                    DateTimeFormatter.ofPattern(CUSTOM_DATE_TIME_FORMAT)) : null;
        }

        @Override
        public String print(LocalDateTime lt, Locale locale) {
            return lt.format(DateTimeFormatter.ofPattern(CUSTOM_DATE_TIME_FORMAT));
        }
    }
}
