package com.example.yandexacademy.util;

import com.example.yandexacademy.model.ShopUnit;
import com.example.yandexacademy.model.ShopUnitHistory;
import com.example.yandexacademy.model.ShopUnitType;
import com.example.yandexacademy.to.ShopUnitImport;
import com.example.yandexacademy.to.ShopUnitStatisticUnit;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

public class ShopUnitConverter {

    public static ShopUnit covertToShopUnit(ShopUnitImport shopUnitImport, LocalDateTime date) {
        ShopUnitType type = shopUnitImport.getType();
        return new ShopUnit(shopUnitImport.getId(), shopUnitImport.getName(), date, checkNullParentId(shopUnitImport.getParentId()),
                type, shopUnitImport.getPrice(), type == ShopUnitType.CATEGORY ? new HashSet<>() : null);
    }

    public static List<ShopUnit> convertToShopUnit(List<ShopUnitImport> importList, LocalDateTime date) {
        return importList.stream()
                .map(shopUnitImport -> covertToShopUnit(shopUnitImport, date))
                .toList();
    }

    public static String checkNullParentId(String parentId) {
        return parentId == null || parentId.isEmpty() || parentId.equalsIgnoreCase("none")
                || parentId.equalsIgnoreCase("null") ? null : parentId;
    }

    public static ShopUnitStatisticUnit convertToStatisticUnit(ShopUnit shopUnit) {
        return new ShopUnitStatisticUnit(shopUnit.getId(), shopUnit.getName(), shopUnit.getParentId(),
                shopUnit.getType(), shopUnit.getPrice(), shopUnit.getDate());
    }

    public static List<ShopUnitStatisticUnit> convertToStatisticUnit(List<ShopUnit> listShopUnit) {
        return listShopUnit.stream()
                .map(ShopUnitConverter::convertToStatisticUnit)
                .toList();
    }

    public static List<ShopUnitStatisticUnit> convertToStatisticUnit(ShopUnit shopUnit, List<ShopUnitHistory> history) {
        return history.stream()
                .map(h -> new ShopUnitStatisticUnit(shopUnit.getId(), shopUnit.getName(), shopUnit.getParentId(),
                        shopUnit.getType(), h.getPrice(), h.getDate()))
                .toList();
    }
}
