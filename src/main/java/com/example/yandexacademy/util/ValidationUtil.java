package com.example.yandexacademy.util;

import com.example.yandexacademy.exception.IllegalRequestDataException;
import com.example.yandexacademy.model.ShopUnit;
import com.example.yandexacademy.model.ShopUnitType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED_MESSAGE;

public class ValidationUtil {

    public static final String UUID_REGEX = "[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}";

    public static void valid(List<ShopUnit> unitList) {
        duplicateValid(unitList);
        nameValid(unitList);
        priceValid(unitList);
        parentValidInList(unitList);
    }

    private static void duplicateValid(List<ShopUnit> unitList) {
        Set<String> ids = new HashSet<>();
        for (ShopUnit unit : unitList) {
            ids.add(unit.getId());
        }
        if (unitList.size() != ids.size()) {
            throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
        }
    }

    private static void nameValid(List<ShopUnit> unitList) {
        for (ShopUnit unit : unitList) {
            if (unit.getName() == null) {
                throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
            }
        }
    }

    private static void priceValid(List<ShopUnit> unitList) {
        for (ShopUnit unit : unitList) {
            Integer price = unit.getPrice();
            if (unit.getType() == ShopUnitType.CATEGORY && price != null) {
                throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
            } else if (unit.getType() == ShopUnitType.OFFER && (price == null || price < 0)) {
                throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
            }
        }
    }

    private static void parentValidInList(List<ShopUnit> unitList) {
        Set<String> offers = new HashSet<>();
        for (ShopUnit unit : unitList) {
            if (unit.getType() == ShopUnitType.OFFER) {
                offers.add(unit.getId());
            }
        }
        for (ShopUnit unit : unitList) {
            if (offers.contains(unit.getParentId())) {
                throw new IllegalRequestDataException(VALIDATION_FAILED_MESSAGE);
            }
        }
    }
}
