package com.example.yandexacademy.exception;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.bind.validation.BindValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED_MESSAGE;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({IllegalRequestDataException.class, NotFoundUnitException.class})
    public ResponseEntity<?> appException(ResponseStatusException ex) {
        log.error("Application error: {}", ex.getMessage());
        Error error = new Error(ex.getStatus().value(), ex.getReason());
        return new ResponseEntity<>(error, ex.getStatus());
    }

    @ExceptionHandler({MismatchedInputException.class, ConstraintViolationException.class,
            IllegalArgumentException.class, BindValidationException.class})
    public ResponseEntity<?> argumentException(Exception ex) {
        log.error("Application error: {}", ex.getMessage());
        Error error = new Error(HttpStatus.BAD_REQUEST.value(), VALIDATION_FAILED_MESSAGE);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
