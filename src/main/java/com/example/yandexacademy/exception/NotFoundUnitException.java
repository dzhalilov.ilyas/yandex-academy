package com.example.yandexacademy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotFoundUnitException extends ResponseStatusException{
    public NotFoundUnitException(String reason) {
        super(HttpStatus.NOT_FOUND, reason);
    }
}