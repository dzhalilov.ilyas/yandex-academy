package com.example.yandexacademy.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Error {
    private Integer code;
    private String message;
}
