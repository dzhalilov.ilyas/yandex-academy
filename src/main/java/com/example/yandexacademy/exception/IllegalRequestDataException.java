package com.example.yandexacademy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IllegalRequestDataException extends ResponseStatusException {
    public IllegalRequestDataException(String reason) {
        super(HttpStatus.BAD_REQUEST, reason);
    }
}
