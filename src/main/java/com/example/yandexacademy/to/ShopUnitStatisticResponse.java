package com.example.yandexacademy.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ShopUnitStatisticResponse {
    private List<ShopUnitStatisticUnit> items;
}
