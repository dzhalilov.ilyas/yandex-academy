package com.example.yandexacademy.to;

import com.example.yandexacademy.model.ShopUnitType;
import lombok.*;
import org.springframework.data.relational.core.mapping.Embedded;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ShopUnitImport {
    @NotNull
    private String id;

    @NotNull
    private String name;

    private String parentId;

    @NotNull
    private ShopUnitType type;

    private Integer price;
}
