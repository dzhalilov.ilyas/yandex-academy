package com.example.yandexacademy.to;

import com.example.yandexacademy.model.ShopUnitType;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ShopUnitStatisticUnit {

    @NotNull
    private String id;
    @NotNull
    private String name;
    private String parentId;
    @NotNull
    private ShopUnitType type;
    private Integer price;
    @NotNull
    private LocalDateTime date;
}
