package com.example.yandexacademy.model;

public enum ShopUnitType {
    OFFER,
    CATEGORY
}
