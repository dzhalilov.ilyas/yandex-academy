package com.example.yandexacademy.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class ShopUnitHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String shopUnitId;

    private LocalDateTime date;

    private Integer price;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shopUnitId", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ToString.Exclude
    private ShopUnit shopUnit;

    public ShopUnitHistory(ShopUnit shopUnit, LocalDateTime date, Integer price) {
        this.shopUnitId = shopUnit.getId();
        this.date = date;
        this.price = price;
        this.shopUnit = shopUnit;
    }
}
