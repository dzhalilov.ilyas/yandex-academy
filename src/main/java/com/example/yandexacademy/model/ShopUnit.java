package com.example.yandexacademy.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.Set;

import static com.example.yandexacademy.util.ValidationUtil.UUID_REGEX;

@Entity
@Table(name = "shop_unit", uniqueConstraints = {@UniqueConstraint(name = "uniq_id", columnNames = "id")})
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ShopUnit {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @Pattern(regexp = UUID_REGEX)
    private String id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDateTime date;

    @Pattern(regexp = UUID_REGEX)
    private String parentId;

    @Column(nullable = false)
    private ShopUnitType type;

    private Integer price;

    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<ShopUnit> children;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, targetEntity = ShopUnitHistory.class)
    @JoinColumn(name = "shopUnitId")
    @ToString.Exclude
    @JsonIgnore
    private Set<ShopUnitHistory> history;

    public ShopUnit(String id, String name, LocalDateTime date, String parentId, ShopUnitType type, Integer price, Set<ShopUnit> children) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.parentId = parentId;
        this.type = type;
        this.price = price;
        this.children = children;
    }

    public void setChildren(Set<ShopUnit> children) {
        if (children == null || this.children == null) {
            this.children = children;
        } else {
            this.children.clear();
            this.children.addAll(children);
        }
    }
}
