package com.example.yandexacademy.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PriceCategoryCounter {
    private int price;
    private int count;
}
