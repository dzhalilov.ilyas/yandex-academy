package com.example.yandexacademy.controller.test;

import com.example.yandexacademy.controller.AbstractMarketControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.example.yandexacademy.controller.MarketControllerTestData.*;
import static com.example.yandexacademy.util.ShopUnitConverter.convertToStatisticUnit;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StatisticTest extends AbstractMarketControllerTest {
    protected StatisticTest(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Test
    void getAllStatistic() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(OFFER_WITH_CATEGORY_ID_FOR_HISTORY1))))
                .andDo(print())
                .andExpect(status().isOk());
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(OFFER_WITH_CATEGORY_ID_FOR_HISTORY2))))
                .andDo(print())
                .andExpect(status().isOk());

        perform(MockMvcRequestBuilders.get(URL_NODE + OFFER_2_ID + URL_STATISTIC)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(STATISTIC_UNIT_MATCHER.contentJson(List.of(convertToStatisticUnit(OFFER_FOR_HISTORY2),
                        convertToStatisticUnit(OFFER_FOR_HISTORY1), convertToStatisticUnit(OFFER_2))));
    }

    @Test
    void getStatisticBetweenDate() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_NODE + CATEGORY_ТЕЛЕВИЗОРЫ_ID + URL_STATISTIC)
                .param("dateStart", "2022-02-03T12:00:00.000Z")
                .param("dateEnd", "2022-02-03T15:00:00.000Z")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(STATISTIC_UNIT_MATCHER.contentJson(List.of(STATISTIC_UNIT_2, STATISTIC_UNIT_1)));
    }

    @Test
    void getStatisticWithWrongDateFormat() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_NODE + CATEGORY_ТЕЛЕВИЗОРЫ_ID + URL_STATISTIC)
                .param("dateStart", "2022-02-03T12:00:00")
                .param("dateEnd", "2022-02-03T15:00:00.000Z")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void getNotFoundStatistic() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_NODE + OFFER_1_ID + URL_STATISTIC)
                .param("dateStart", "2022-02-03T12:00:00.000Z")
                .param("dateEnd", "2022-02-03T15:00:00.000Z")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void getBeforeTheLastRootPrice() throws Exception {
        perform(MockMvcRequestBuilders.delete(URL_DELETE_ITEM + OFFER_GOLD_STAR_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertEquals(EXPECTED_ROOT_PRICE_BEFORE_THE_LAST,
                historyRepository.findAllByShopId(ROOT_ITEM_ID).get(1).getPrice());
    }
}
