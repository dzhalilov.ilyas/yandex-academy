package com.example.yandexacademy.controller.test;

import com.example.yandexacademy.controller.AbstractMarketControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.yandexacademy.controller.MarketController.NOT_FOUND_NODES;
import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED;
import static com.example.yandexacademy.controller.MarketControllerTestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DeleteTest extends AbstractMarketControllerTest {

    protected DeleteTest(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Test
    void deleteOffer() throws Exception {
        perform(MockMvcRequestBuilders.delete(URL_DELETE_ITEM + OFFER_1_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertFalse(repository.findById(OFFER_1_ID).isPresent());
        assertEquals(EXPECTED_CATEGORY1_PRICE_AFTER_DELETE_OFFER1, repository.findById(CATEGORY_СМАРТФОНЫ_ID).get().getPrice());
        assertEquals(EXPECTED_ROOT_PRICE_AFTER_DELETE_OFFER1, repository.findById(ROOT_ITEM_ID).get().getPrice());
    }

    @Test
    void deleteCategory() throws Exception {
        perform(MockMvcRequestBuilders.delete(URL_DELETE_ITEM + CATEGORY_СМАРТФОНЫ_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertFalse(repository.findById(CATEGORY_СМАРТФОНЫ_ID).isPresent());
        assertFalse(repository.findById(OFFER_1_ID).isPresent());
        assertFalse(repository.findById(OFFER_2_ID).isPresent());
        assertEquals(1, repository.getChildrenById(ROOT_ITEM_ID).size());
        assertEquals(EXPECTED_ROOT_PRICE_AFTER_DELETE_CATEGORY1, repository.findById(ROOT_ITEM_ID).get().getPrice());
    }

    @Test
    void deleteNotFoundItem() throws Exception {
        perform(MockMvcRequestBuilders.delete(URL_DELETE_ITEM + NOT_FOUND_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(ERROR_MATCHER.contentJson(NOT_FOUND_NODES));
        assertEquals(EXPECTED_ROOT_PRICE, repository.findById(ROOT_ITEM_ID).get().getPrice());
    }

    @Test
    void deleteNotValid() throws Exception {
        perform(MockMvcRequestBuilders.delete(URL_DELETE_ITEM + NOT_VALID_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
        assertEquals(EXPECTED_ROOT_PRICE, repository.findById(ROOT_ITEM_ID).get().getPrice());
    }
}
