package com.example.yandexacademy.controller.test;

import com.example.yandexacademy.controller.AbstractMarketControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED;
import static com.example.yandexacademy.controller.MarketControllerTestData.*;
import static com.example.yandexacademy.util.ShopUnitConverter.convertToStatisticUnit;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SalesTest extends AbstractMarketControllerTest {

    protected SalesTest(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Test
    void salesNotValidDate() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_SALES)
                .param("date", WRONG_DATE_TIME_FORMAT))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void sales() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_SALES)
                .param("date", DATE_TIME_FOR_CATEGORY_СМАРТФОНЫ))
                .andExpect(status().isOk())
                .andExpect(STATISTIC_UNIT_MATCHER.contentJson(convertToStatisticUnit(OFFER_1),
                        convertToStatisticUnit(OFFER_2), convertToStatisticUnit(CATEGORY_1)));
    }

    @Test
    void salesEmpty() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_SALES)
                .param("date", DATE_TIME_FOR_WITH_NO_SALES))
                .andExpect(status().isOk())
                .andExpect(STATISTIC_UNIT_MATCHER.contentJson());
    }
}
