package com.example.yandexacademy.controller.test;

import com.example.yandexacademy.controller.AbstractMarketControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.yandexacademy.controller.MarketController.NOT_FOUND_NODES;
import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED;
import static com.example.yandexacademy.controller.MarketControllerTestData.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NodesTest extends AbstractMarketControllerTest {

    protected NodesTest(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Test
    void getOfferItem() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_GET_ITEM + OFFER_1_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(SHOP_UNIT_MATCHER.contentJson(OFFER_1));
    }

    @Test
    void getCategoryItem() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_GET_ITEM + CATEGORY_СМАРТФОНЫ_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(SHOP_UNIT_MATCHER.contentJson(CATEGORY_1));
    }

    @Test
    void getWithNotFoundId() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_GET_ITEM + NOT_FOUND_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(ERROR_MATCHER.contentJson(NOT_FOUND_NODES));
    }

    @Test
    void getWithNotValidId() throws Exception {
        perform(MockMvcRequestBuilders.get(URL_GET_ITEM + NOT_VALID_ID)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }
}
