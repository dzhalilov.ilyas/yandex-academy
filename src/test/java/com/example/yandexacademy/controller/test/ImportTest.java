package com.example.yandexacademy.controller.test;

import com.example.yandexacademy.controller.AbstractMarketControllerTest;
import com.example.yandexacademy.model.ShopUnit;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.yandexacademy.controller.MarketController.VALIDATION_FAILED;
import static com.example.yandexacademy.controller.MarketControllerTestData.*;
import static com.example.yandexacademy.controller.MarketControllerTestData.ERROR_MATCHER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ImportTest extends AbstractMarketControllerTest {
    public ImportTest(WebApplicationContext webApplicationContext) {
        super(webApplicationContext);
    }

    @Test
    void importData() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(IMPORT_DATA))))
                .andDo(print())
                .andExpect(status().isOk());
        ShopUnit shopUnit = repository.findById(ROOT_ITEM_ID).get();
        assertEquals(EXPECTED_COUNT_OF_CHILDREN, repository.getChildrenById(ROOT_ITEM_ID).size());
        assertEquals(EXPECTED_ROOT_PRICE_AFTER_IMPORT_NEW_CATEGORY, shopUnit.getPrice());
    }

    @Test
    void importDataWithDuplicateId() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(DUPLICATE_ID))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithWrongOfferPrice() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(WRONG_OFFER_PRICE))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithNotValidRequest() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(IMPORT_BATCHES))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithNullOfferPrice() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(NULL_OFFER_PRICE))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithNotNullCategoryPrice() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(NOT_NULL_CATEGORY_PRICE))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithNullName() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(NULL_NAME))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithOfferWithCategoryId() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(OFFER_WITH_CATEGORY_ID))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void importDataWithWrongDateFormat() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(WRONG_DATE_FORMAT))))
                .andExpect(status().isBadRequest())
                .andExpect(ERROR_MATCHER.contentJson(VALIDATION_FAILED));
    }

    @Test
    void checkRootCategoryPriceAfterUpdatingOffer() throws Exception {
        perform(MockMvcRequestBuilders.post(URL_IMPORTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(parser.parse(OFFER_WITH_CATEGORY_ID_FOR_HISTORY2))))
                .andDo(print())
                .andExpect(status().isOk());

        assertEquals(88749, repository.findById(CATEGORY_СМАРТФОНЫ_ID).get().getPrice());
        assertEquals(66099, repository.findById(ROOT_ITEM_ID).get().getPrice() );
    }
}
