package com.example.yandexacademy.controller;

import com.example.yandexacademy.exception.Error;
import com.example.yandexacademy.model.ShopUnit;
import com.example.yandexacademy.model.ShopUnitType;
import com.example.yandexacademy.to.ShopUnitStatisticUnit;

import java.time.LocalDateTime;
import java.util.Set;

public class MarketControllerTestData {
    public static final MatcherFactory.Matcher<ShopUnit> SHOP_UNIT_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(ShopUnit.class);
    public static final MatcherFactory.Matcher<ShopUnitStatisticUnit> STATISTIC_UNIT_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(ShopUnitStatisticUnit.class);
    public static final MatcherFactory.Matcher<Error> ERROR_MATCHER = MatcherFactory.usingIgnoringFieldsComparator(Error.class);

    public static final String ROOT_ITEM_ID = "069cb8d7-bbdd-47d3-ad8f-82ef4c269df1";
    public static final String OFFER_1_ID = "863e1a7a-1304-42ae-943b-179184c077e3";
    public static final String OFFER_2_ID = "b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4";
    public static final String OFFER_GOLD_STAR_ID = "73bc3b36-02d1-4245-ab35-3106c9ee1c65";
    public static final String CATEGORY_ТЕЛЕВИЗОРЫ_ID = "1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2";
    public static final String CATEGORY_СМАРТФОНЫ_ID = "d515e43f-f3f6-4471-bb77-6b455017a2d2";
    public static final String NOT_FOUND_ID = "d515e43f-f3f6-4471-bb77-6b455017a000";
    public static final String NOT_VALID_ID = "d515e43f-f3f644-71-bb77-6b455017a000";
    public static final Integer EXPECTED_COUNT_OF_CHILDREN = 3;
    public static final Integer EXPECTED_ROOT_PRICE = 58599;
    public static final Integer EXPECTED_ROOT_PRICE_BEFORE_THE_LAST = 55749;
    public static final Integer EXPECTED_ROOT_PRICE_AFTER_IMPORT_NEW_CATEGORY = 73285;
    public static final Integer EXPECTED_ROOT_PRICE_AFTER_DELETE_CATEGORY1 = 50999;
    public static final Integer EXPECTED_ROOT_PRICE_AFTER_DELETE_OFFER1 = 53249;
    public static final Integer EXPECTED_CATEGORY1_PRICE_AFTER_DELETE_OFFER1 = 59999;
    public static final String WRONG_DATE_TIME_FORMAT = "07-05-2022T12:00:00";
    public static final String DATE_TIME_FOR_CATEGORY_СМАРТФОНЫ = "2022-02-03T09:00:00.000Z";
    public static final String DATE_TIME_FOR_WITH_NO_SALES = "2022-02-05T12:00:00.000Z";
    public static final String IMPORT_BATCHES = "[\n" +
            "    {\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"CATEGORY\",\n" +
            "                \"name\": \"Товары\",\n" +
            "                \"id\": \"069cb8d7-bbdd-47d3-ad8f-82ef4c269df1\",\n" +
            "                \"parentId\": null\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-01T12:00:00.000Z\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"CATEGORY\",\n" +
            "                \"name\": \"Смартфоны\",\n" +
            "                \"id\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"parentId\": \"069cb8d7-bbdd-47d3-ad8f-82ef4c269df1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"jPhone 13\",\n" +
            "                \"id\": \"863e1a7a-1304-42ae-943b-179184c077e3\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 79999\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"CATEGORY\",\n" +
            "                \"name\": \"Телевизоры\",\n" +
            "                \"id\": \"1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2\",\n" +
            "                \"parentId\": \"069cb8d7-bbdd-47d3-ad8f-82ef4c269df1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Samson 70\\\" LED UHD Smart\",\n" +
            "                \"id\": \"98883e8f-0507-482f-bce2-2fb306cf6483\",\n" +
            "                \"parentId\": \"1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2\",\n" +
            "                \"price\": 32999\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Phyllis 50\\\" LED UHD Smarter\",\n" +
            "                \"id\": \"74b81fda-9cdc-4b63-8927-c978afed5cf4\",\n" +
            "                \"parentId\": \"1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2\",\n" +
            "                \"price\": 49999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-03T12:00:00.000Z\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Goldstar 65\\\" LED UHD LOL Very Smart\",\n" +
            "                \"id\": \"73bc3b36-02d1-4245-ab35-3106c9ee1c65\",\n" +
            "                \"parentId\": \"1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2\",\n" +
            "                \"price\": 69999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-03T15:00:00.000Z\"\n" +
            "    }\n" +
            "]";
    public static final String DUPLICATE_ID = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"CATEGORY\",\n" +
            "                \"name\": \"Смартфоны\",\n" +
            "                \"id\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"parentId\": \"069cb8d7-bbdd-47d3-ad8f-82ef4c269df1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"jPhone 13\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 79999\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    }";
    public static final String IMPORT_DATA = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"CATEGORY\",\n" +
            "                \"name\": \"Холодильники\",\n" +
            "                \"id\": \"d515e43f-0000-4471-bb77-6b455017r9r2\",\n" +
            "                \"parentId\": \"069cb8d7-bbdd-47d3-ad8f-82ef4c269df1\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"HAIR LUX\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af9ft65\",\n" +
            "                \"parentId\": \"d515e43f-0000-4471-bb77-6b455017r9r2\",\n" +
            "                \"price\": 100000\n" +
            "            },\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Samsung S3159\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af98765\",\n" +
            "                \"parentId\": \"d515e43f-0000-4471-bb77-6b455017r9r2\",\n" +
            "                \"price\": 120000\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-05T12:00:00.000Z\"\n" +
            "    }";
    public static final String WRONG_OFFER_PRICE = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": -59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    }";
    public static final String NULL_OFFER_PRICE = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": null\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    }";
    public static final String NOT_NULL_CATEGORY_PRICE = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"CATEGORY\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    }";
    public static final String NULL_NAME = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": null,\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    }";
    public static final String OFFER_WITH_CATEGORY_ID = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"1cc0129a-2bfe-474c-9ee6-d435bf5fc8f2\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-02T12:00:00.000Z\"\n" +
            "    }";
    public static final String OFFER_WITH_CATEGORY_ID_FOR_HISTORY1 = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 45000\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-05T12:00:00.000Z\"\n" +
            "    }";
    public static final String OFFER_WITH_CATEGORY_ID_FOR_HISTORY2 = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 97500\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"2022-02-28T12:00:00.000Z\"\n" +
            "    }";
    public static final String WRONG_DATE_FORMAT = "{\n" +
            "        \"items\": [\n" +
            "            {\n" +
            "                \"type\": \"OFFER\",\n" +
            "                \"name\": \"Xomiа Readme 10\",\n" +
            "                \"id\": \"b1d8fd7d-2ae3-47d5-b2f9-0f094af800d4\",\n" +
            "                \"parentId\": \"d515e43f-f3f6-4471-bb77-6b455017a2d2\",\n" +
            "                \"price\": 59999\n" +
            "            }\n" +
            "        ],\n" +
            "        \"updateDate\": \"07-05-2022T12:00:00\"\n" +
            "    }";

    public static final ShopUnit OFFER_1 = new ShopUnit(OFFER_1_ID, "jPhone 13",
            LocalDateTime.of(2022, 2, 2, 12, 0, 0),
            CATEGORY_СМАРТФОНЫ_ID, ShopUnitType.OFFER, 79999, null);

    public static final ShopUnit OFFER_2 = new ShopUnit(OFFER_2_ID, "Xomiа Readme 10",
            LocalDateTime.of(2022, 2, 2, 12, 0, 0),
            CATEGORY_СМАРТФОНЫ_ID, ShopUnitType.OFFER, 59999, null);

    public static final ShopUnit CATEGORY_1 = new ShopUnit(CATEGORY_СМАРТФОНЫ_ID, "Смартфоны",
            LocalDateTime.of(2022, 2, 2, 12, 0, 0),
            ROOT_ITEM_ID, ShopUnitType.CATEGORY, 69999, Set.of(OFFER_2, OFFER_1));

    public static final ShopUnit OFFER_FOR_HISTORY1 = new ShopUnit(OFFER_2_ID, "Xomiа Readme 10",
            LocalDateTime.of(2022, 2, 5, 12, 0, 0),
            CATEGORY_СМАРТФОНЫ_ID, ShopUnitType.OFFER, 45000, null);

    public static final ShopUnit OFFER_FOR_HISTORY2 = new ShopUnit(OFFER_2_ID, "Xomiа Readme 10",
            LocalDateTime.of(2022, 2, 28, 12, 0, 0),
            CATEGORY_СМАРТФОНЫ_ID, ShopUnitType.OFFER, 97500, null);

    public static final ShopUnitStatisticUnit STATISTIC_UNIT_1 = new ShopUnitStatisticUnit(CATEGORY_ТЕЛЕВИЗОРЫ_ID, "Телевизоры",
            ROOT_ITEM_ID, ShopUnitType.CATEGORY, 50999, LocalDateTime.of(2022, 2, 3, 15, 0, 0));

    public static final ShopUnitStatisticUnit STATISTIC_UNIT_2 = new ShopUnitStatisticUnit(CATEGORY_ТЕЛЕВИЗОРЫ_ID, "Телевизоры",
            ROOT_ITEM_ID, ShopUnitType.CATEGORY, 41499, LocalDateTime.of(2022, 2, 3, 12, 0, 0));
}

