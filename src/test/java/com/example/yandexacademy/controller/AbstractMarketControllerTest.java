package com.example.yandexacademy.controller;

import com.example.yandexacademy.repository.HistoryRepository;
import com.example.yandexacademy.repository.ShopUnitRepository;
import com.example.yandexacademy.service.MarketService;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.sql.DataSource;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
abstract public class AbstractMarketControllerTest {

    protected final MockMvc mockMvc;
    protected final String URL_IMPORTS = "http://localhost:80/imports";
    protected final String URL_GET_ITEM = "/nodes/";
    protected final String URL_DELETE_ITEM = "/delete/";
    protected final String URL_SALES = "/sales";
    protected final String URL_NODE = "/node/";
    protected final String URL_STATISTIC = "/statistic";

    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected ShopUnitRepository repository;

    @Autowired
    protected HistoryRepository historyRepository;

    @Autowired
    protected MarketService service;

    protected final JSONParser parser = new JSONParser();

    protected AbstractMarketControllerTest(WebApplicationContext webApplicationContext) {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .addFilter((servletRequest, servletResponse, filterChain) -> {
                    servletResponse.setCharacterEncoding("UTF-8");
                    filterChain.doFilter(servletRequest, servletResponse);
                })
                .build();
    }

    protected ResultActions perform(MockHttpServletRequestBuilder builder) throws Exception {
        return mockMvc.perform(builder);
    }

    @BeforeEach
    void initDatabase() {
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScript(new ClassPathResource("/init.sql"));
        populator.addScript(new ClassPathResource("/data.sql"));
        populator.execute(dataSource);
    }
}